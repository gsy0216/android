package com.example.myprovider;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class MyDAO {
    private Context context;
    private SQLiteDatabase database;

    public MyDAO(Context context){
        this.context = context;
        MyDBHelper dbHelper = new MyDBHelper(context,"lcDB",null,1);
        database = dbHelper.getWritableDatabase();
    }


    public Uri lcInsert(ContentValues values){
        long rowId = database.insert("student",null,values);
        Uri uri = Uri.parse("content://lc.provider1/student");

        Uri insertUri = ContentUris.withAppendedId(uri,rowId);
        context.getContentResolver().notifyChange(insertUri,null);
        return insertUri;
    }
}
