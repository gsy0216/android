package com.example.mywork_1;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ItemActivity extends AppCompatActivity {
    private Button btn_getInfo;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("ItemActivity","onStart()被调用");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ItemActivity","onResume()被调用");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("ItemActivity","onPause()被调用");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("ItemActivity","onStop()被调用");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("ItemActivity","onDestroy()被调用");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("ItemActivity","onRestart()被调用");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        Log.d("ItemActivity","onCreate()被调用");
        //接收Adapter中向ItemActivity传递的对话对象的name属性
        TextView activity_item_header = findViewById(R.id.activity_item_header);
        Intent intent1 = getIntent();
        activity_item_header.setText(intent1.getStringExtra("name"));
        //点击btn_getInfo按钮配合ActivityResultLauncher获取ResultActivity中的Info
        btn_getInfo = findViewById(R.id.btn_getInfo);
        btn_getInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(ItemActivity.this,ResultActivity.class);
                test.launch(intent2);
            }
        });
    }
    public ActivityResultLauncher test = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == 8080){
                        TextView textView_Info = findViewById(R.id.textView_Info);
                        textView_Info.setText(result.getData().getStringExtra("data"));
                    }
                }
            });
}