package com.example.mywork_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.Calendar;

public class ResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        //向ItemActivity返回Info
        Intent intent = getIntent();
        intent.putExtra("data","未读消息N条");
        setResult(8080,intent);
        finish();
    }
}